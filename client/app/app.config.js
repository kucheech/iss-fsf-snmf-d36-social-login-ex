(function () {
    'use strict';

    angular
        .module('MyApp')
        .config(function (socialProvider) {
            // socialProvider.setGoogleKey("YOUR GOOGLE CLIENT ID");
            // socialProvider.setLinkedInKey("YOUR LINKEDIN CLIENT ID");
            socialProvider.setFbKey({ appId: "287638241738286", apiVersion: "v2.10" });
        });

})();